var move_point = 0;
var overload_power = 0;
var start_point = 0;
var machines = [];
function fetch(url)
{
	$.ajax({
	url: url ,
	}).fail(function() {
		showMessage("Coś poszło nie tak","warning");
	}).done(function(data) {
		$('#content-table').html('');
		data = JSON.parse(data);
		for(var i = 0 ; i < data.length; i++)
		{
			row = ` 
				<div onclick='currents(`+data[i]['id']+`)' class='col border machine' >`+data[i]['machine_name']+`</div>
			`;
			$('.machines').append(row);
			machines[data[i]["id"]] = data[i];
		}
		$('.machine').click(function(){
            $('.machine').removeClass('activeMachine');
            $(this).addClass('activeMachine');
        });
	});
}



function getCurrents(url){
    $('body').append(loadingCard);
	avg = 0;
	powers = 0;
	wkTime = 0;
	mvTime = 0;
	$.ajax({
	url: url,
	}).fail(function() {
        $('.card').remove();		
		showMessage("Wystąpił problem podczas pobierania próbek urządzenia","warning");
	}).done(function(data) {
        $('.card').remove();		
		$('.canvas-container').html('<canvas id="powerChart"></canvas>')
		$('.line').addClass('d-flex');
		data = JSON.parse(data);
		var chartlabels = [];
		var chartData = [];
		var overloadRange = [];
		var startRange = [];
		var workRange = [];
		var currentPowers = 0;
		y = 0;
		length = Object.keys(data).length-1 
		average = 1;
		for(i = 0; i < length ; i++)
		{
		
			if(length > 1000)
			{
				average = 2;
			}
			if(length > 5000)
			{
				average = 5;
			}
			if(length > 10000)
			{
				average = 10;
			}
			
			if(length > 50000)
			{
				average = 50;
			}
			
			if(length > 100000)
			{
				average = 100;
			}

			powers +=  power = data[i]['ampery'] * data[i]['volty'];
		
			if(power >= move_point)
			{
				mvTime++;
			}
			wkTime++;
	
			avg = (powers/length).toFixed(2);
			currentPowers += power;
			if(y == 0 || y == average)
			{
				chartlabels.push(new Date(data[i]['data']).toLocaleString());
				chartData.push((currentPowers/average).toFixed(2));
				overloadRange.push({x:0,y:overload_power});
				startRange.push({x:0,y:start_point});
				workRange.push({x:0,y:move_point});
				currentPowers = 0;
				y = 0;
			}
			
			y++;
		}
	
		var config = {
			type: 'line',
			data: {
				labels: chartlabels,
				datasets: [{
					label: '#próbka',
					data: chartData,
					backgroundColor: "rgba(173, 216 , 230, 0.2)",
					borderColor: "rgba(0, 0 , 255, 0.2)",
					borderWidth: 1
				},
				{
					label: 'Przekroczenie mocy',
					backgroundColor: "rgba(255, 0 , 0, 0)",
					borderColor: "rgba(255, 0 , 0, 0.2)",
					data: overloadRange,
					// Changes this dataset to become a line
					type: 'scatter'
				},
				{
					label: 'Punkt pracy(ruchu)',
					backgroundColor: "rgba(0, 255 , 0, 0)",
					borderColor: "rgba(0, 255 , 0, 0.2)",
					data: workRange,
					// Changes this dataset to become a line
					type: 'scatter'
				},
				{
					label: 'Punkt uruchomienia(czuwania)',
					backgroundColor: "rgba(0, 0 , 255, 0)",
					borderColor: "rgba(0, 0 , 255, 0.2)",
					data: startRange,
					// Changes this dataset to become a line
					type: 'scatter'
				}
				]
			},
			options: {
				responsive: true,
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 't[h]'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'P[kW]'
						}
					}]
				},
				elements: { point: { radius: 0 } },

			}
		};
		
		var ctx = document.getElementById('powerChart').getContext('2d');
		window.myLine = new Chart(ctx, config);
		
		$('#avg_val').text(avg);
	
		$('#wktm_val').text((wkTime/3600).toFixed(2));
	
		$('#mvtm_val').text((mvTime/3600).toFixed(2));
	
		$('#sqct_val').text(data['sequenceCount']);
		
	});
}