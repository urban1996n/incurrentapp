var overloads = [[]];

function getCurrents(url){
    $('body').append(loadingCard);
	$.ajax({
	url: url,
	}).fail(function() {
        $('.card').remove();		
		showMessage("Wystąpił problem podczas pobierania próbek urządzenia","warning");
	}).done(function(data) {
        $('.card').remove();				
		$('.line').addClass('d-block');
		data = JSON.parse(data);
		
		$('.rows').html('');
	
		for(i = 0; i < Object.keys(data).length ; i++)
		{	
			id= data[i]['id'];
			var power = 0;
			var overloadTime = 0;
			var overloadPower = 0;
			overloads[id] = [];					
			overloadPower = data[i]['overload_power'];
			name = data[i]['machine_name'];
		
			y = 0;
			
			for(y ; y < data[i]['currents'].length; y++)
			{
				power = data[i]['currents'][y]['ampery'] * data[i]['currents'][y]['volty'];
				
				if(power >= overloadPower)
				{
					overloadTime++;
					overloads[data[i]['id']].push(data[i]['currents'][y]['data']);
				}
			}
		
			row =`
				<div class='row'>
				<div class='col-2 border machine' onclick='show(`+data[i]['id']+`)'>`+name+`</div>
				<div class='col-2 border'>`+(overloadTime/3600).toFixed(2)+`</div>
				<div class='col-8'></div>
				</div>
			`;

			$('.rows').append(row);
			console.log(overloadPower);
			console.log(overloads[data[i]['id']]);
		}				
		
	});
}

function show(id)
{
	$('.list').html('');
	for(i = 0; i < overloads[id].length; i++)
	{
		row = 
		`
			<div class='col-12'>`+overloads[id][i]+`</div>
		`;
		$('.list').append(row);
	}
	
	$('.list').addClass('d-block');
}