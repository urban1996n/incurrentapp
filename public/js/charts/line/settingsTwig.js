var fetch_url;
var setSequence_url;	
var edit_url;
var delete_url;
var curr_machine_id;
var _start_point;
var _move_point;
var _overload_power;
var dates;

function fetch_currents(url){
    $('body').append(loadingCard);
	dates = [];
	pointX = [];
	powers = 0;
	wkTime = 0;
	mvTime = 0;
	$.ajax({
	url: url,
	}).fail(function() {
        $('.card').remove();				
		showMessage("Wystąpił błąd podczas pobierania próbek urządzenia","warning");
	}).done(function(data) {
        $('.card').remove();				
		$('.canvas-container').html('<canvas id="powerChart"></canvas>')
		$('.line').addClass('d-flex');
		data = JSON.parse(data);
		var chartlabels = [];
		var chartData = [];
		var overloadRange = [];
		var startRange = [];
		var workRange = [];
		var currentPowers = 0;
		y = 0;
		length = Object.keys(data).length-1 
		average = 1;
		for(i = 0; i < length ; i++)
		{
		
			if(length > 1000)
			{
				average = 2;
			}
			if(length > 5000)
			{
				average = 5;
			}
			if(length > 10000)
			{
				average = 10;
			}
			
			if(length > 50000)
			{
				average = 50;
			}
			
			if(length > 100000)
			{
				average = 100;
			}

			powers +=  power = data[i]['ampery'] * data[i]['volty'];
		
			if(power >= move_point)
			{
				mvTime++;
			}
			wkTime++;
	
			avg = (powers/length).toFixed(2);
			currentPowers += power;
			if(y == 0 || y == average)
			{
				chartlabels.push(new Date(data[i]['data']).toLocaleString());
				chartData.push((currentPowers/average).toFixed(2));
				overloadRange.push({x:0,y:_overload_power});
				startRange.push({x:0,y:_start_point});
				workRange.push({x:0,y:_move_point});
				currentPowers = 0;
				y = 0;
			}
			
			y++;
		}
	
		var config = {
			type: 'line',
			data: {
				labels: chartlabels,
				datasets: [{
					label: '#próbka',
					data: chartData,
					backgroundColor: "rgba(173, 216 , 230, 0.2)",
					borderColor: "rgba(0, 0 , 255, 0.2)",
					borderWidth: 1
				},
				{
					label: 'Przekroczenie mocy',
					backgroundColor: "rgba(255, 0 , 0, 0)",
					borderColor: "rgba(255, 0 , 0, 0.2)",
					data: overloadRange,
					// Changes this dataset to become a line
					type: 'scatter'
				},
				{
					label: 'Punkt pracy(ruchu)',
					backgroundColor: "rgba(0, 255 , 0, 0)",
					borderColor: "rgba(0, 255 , 0, 0.2)",
					data: workRange,
					// Changes this dataset to become a line
					type: 'scatter'
				},
				{
					label: 'Punkt uruchomienia(czuwania)',
					backgroundColor: "rgba(0, 0 , 255, 0)",
					borderColor: "rgba(0, 0 , 255, 0.2)",
					data: startRange,
					// Changes this dataset to become a line
					type: 'scatter'
				}
				]
			},
			options: {
				responsive: true,
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 't[h]'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'P[kW]'
                        },
					
                    }]              
				},
				elements: { point: { radius: 0 } },
				
                onClick:function(evt){
					$('#setSequence').prop('disabled',true);
					points = window.myLine.getElementsAtEvent(evt);
					chart = document.getElementById('powerChart');
					var top = chart.offsetTop + chart.height-10;
					var left = evt.pageX;
					try{
						if(dates.length == 2)
						{
							pointX = [];
							dates = [];
							$('#dateFromLine').remove();
							$('#dateToLine').remove();
							$('#graphDiv').remove();
						}
						dates.push(window.myLine.data.labels[points[0]._index]);
						pointX.push(left);
						
					}catch(err)
					{
						$('#graphDiv').remove();
						return false;
					}
					
					if(dates.length == 1)
					{
						var line = `<i id='dateFromLine' style='height:2px;width:20px; color:orange; position:absolute; top:`+top+`px; left:`+left+`px'>Od</i>`;
						$('.tips').append(line);
					
					}
					if(dates.length == 2)
					{
						var line = `<i id='dateToLine' style='height:2px;width:20px; color:orange; position:absolute; top:`+top+`px; left:`+left+`px'>Do</i>`;
						
						if(pointX[1] > pointX[0])
						{
							blockLeft = pointX[0];
							blockRight = pointX[1];
							blockWidth =Math.abs(blockRight-blockLeft);
						}else{
							blockRight = pointX[0];
							blockLeft = pointX[1];
							blockWidth =Math.abs(blockLeft-blockRight);
						}

						top = top - 470;
						var block = `<div id='graphDiv' style='background:rgba(255,127,80,0.1); z-index:999999999; position:absolute; top:`+top+`px ; left:`+blockLeft+`px ;height:`+(parseInt(chart.height)-90)+`px ;width:`+blockWidth+`px;'></div>`;
						$('body').append(block);
						$('.tips').append(line);
						$('#setSequence').prop('disabled',false);
					}
                }
			}
		};
		var ctx = document.getElementById('powerChart').getContext('2d');
		window.myLine = new Chart(ctx, config);		
	});
}

_ids = Array();
var move_point;

function fetch()
{
    $.ajax({
    url: fetch_url,
    }).fail(function() {
        showMessage("Wystąpił błąd podczas pobierania informacji o urządzeniach","warning");
    }).done(function(data) {
        $('#content-box').html('');
        data = JSON.parse(data);
        for(var i = 0 ; i < data.length; i++)
        {   
            _ids.push(data[i]['id']);

            row = ` 
                <div class='border col-2' ><input class='form-control' type="text" id='machine_name`+data[i]['id']+`' value='`+data[i]['machine_name']+`' name='machine_name'></div>
                <div class='border col-2' ><input class='form-control' required type='number' step="0.01" id='start_point`+data[i]['id']+`' value='`+data[i]['start_point']+`' style='width:100%' name='start_point'></div>
                <div class='border col-2' ><input class='form-control' required type='number' step="0.01" id='move_point`+data[i]['id']+`' value='`+data[i]['move_point']+`' style='width:100%' name='move_point'></div>
                <div class='border col-2' ><input class='form-control' required type='number' step="0.01" id='overload_power`+data[i]['id']+`' value='`+data[i]['overload_power']+`' style='width:100%' name='overload_power'></div>
                <div class='border col-2' ><input class='form-control' required  type='number' id='Id`+data[i]['id']+`'  value='`+data[i]['id']+`' style='width:100%' name='Id'></div>
                <div class='border col-2' >
                    <button onClick="edit(`+data[i]['id']+`,'`+data[i]['username']+`')" class='btn btn-success btn-edit' data-id='`+data[i]['id']+`' type='button' style='width:100%; margin-bottom:8px;'>Zmień</button><br/>
                    <button onClick="deleteOne(`+data[i]['id']+`)" class='btn btn-danger btn-edit' data-id='`+data[i]['id']+`' type='button' style='width:100%;'>Usuń</button>
                </div>
            `;
            $('#content-box').append(row);

            row = ` 
            <div onclick='currents(`+data[i]['id']+`,`+data[i]['move_point']+`,`+data[i]['start_point']+`,`+data[i]['overload_power']+`)'  class='col border machine' >`+data[i]['machine_name']+`</div>
            `;
            $('.machines').append(row);
		}
		$('.machine').click(function(){
            $('.machine').removeClass('activeMachine');
            $(this).addClass('activeMachine');
        });
    });
}

$('#btn-add').click(function(){
      
	machine_name = $('#machine_name').val(); 
	start_point = $('#start_point').val();
	move_point = $('#move_point').val();
	overload_power = $('#overload_power').val();
	id = parseInt($('#Id').val());
	var add_url = $(this).attr('data-url');
	if(_ids.includes(id))
	{
		showMessage("Istnieje maszyna, o takim ID w bazie danych, wybierz inny numer identyfikacyjny.","danger");
		return false;
	}
	
	$.ajax({
	async: false,
	url: add_url,
	data: {machine_name:machine_name,start_point:start_point,move_point:move_point,overload_power:overload_power,id:id},
	}).fail(function() {
		showMessage("Wystąpił błąd podczas dodawania maszyny","warning");
	}).done(function(data) {
		showMessage("Maszyna została dodana!","success");
		fetch();
	});
});

function edit(oldid){
    
    machine_name = $('#machine_name'+oldid).val(); 
    start_point = $('#start_point'+oldid).val();
    move_point = $('#move_point'+oldid).val();
    overload_power = $('#overload_power'+oldid).val();
    id = parseInt($('#Id'+oldid).val());
    if(_ids.includes(id) && id != oldid)
    {
        showMessage("Istnieje maszyna, o takim ID w bazie danych, wybierz inny numer identyfikacyjny.","danger");
        return false;
    }
   
    $.ajax({
    async: true,
    type:'POST',
    url: edit_url,
    data: {machine_name:machine_name,start_point:start_point,move_point:move_point,overload_power:overload_power,id:id,oldid:oldid},
    }).fail(function() {
        showMessage("Wystąpił błąd podczas edycji danych maszyny","warning");
    }).done(function(data) {
        showMessage("Dane maszyny zmienione!","success");
        fetch();
    });
}

function deleteOne(id){
    id = id
    var choice = confirm("Czy na pewno chcesz usunąć ten wpis?");
    if(choice == true)
    {
        $.ajax({
        async: true,
        url: delete_url,
        data: {id:id},
        }).fail(function() {
            showMessage("Wystąpił błąd podczas usuwania maszyny","warning");
        }).done(function(data) {
            showMessage("Usunięto maszynę!","success");
            fetch();
        });
    }       
}

$('#setSequence').on('click', function(){
	if(new Date(dates[0]) > new Date(dates[1]))
	{
		startDate = new Date(dates[1]);
		endDate = new Date(dates[0]);
	}else{
		endDate = new Date(dates[1]);
		startDate = new Date(dates[0]);
	}
	$.ajax({
        async: true,
        url: setSequence_url,
        data: {machineId:curr_machine_id, startDate:startDate.toLocaleString(), endDate:endDate.toLocaleString()},
        }).fail(function() {
            showMessage("Wystąpił błąd podczas ustawiania kształtu cyklu","warning");
        }).done(function(data) {
            showMessage("Ustawiono kształt cyklu","success");
        });
});