function getCurrents(url){
    $('body').append(loadingCard);
	$('.rows').html('');
	$.ajax({
	url: url,
	}).fail(function() {
        $('.card').remove();						
		showMessage("Wystąpił problem podczas pobierania próbek urządzenia","warning");
	}).done(function(data) {
        $('.card').remove();				
		$('.line').addClass('d-flex');
		data = JSON.parse(data);

		for(i = 0; i < Object.keys(data).length ; i++)
		{	
			powers = 0;
			wkTime = 0;
			mvTime = 0;
			y = 0;
			for(y ; y < data[i]['currents'].length; y++)
			{
				powers +=  power = data[i]['currents'][y]['ampery'] * data[i]['currents'][y]['volty'];
				if(power >= data[i]['move_point'])
				{
					mvTime++;
				}
				wkTime++;
			}
		
			avg = (powers/y) || 0;

			row =`
			<div class='col-2 border'>`+data[i]['machine_name']+`</div>
			<div class='col-2 border'>`+avg.toFixed(2)+`</div>
			<div class='col-2 border'>`+(wkTime/3600).toFixed(2)+`</div>
			<div class='col-2 border'>`+(mvTime/3600).toFixed(2)+`</div>
			<div class='col-2 border'>`+data[i]['sequence_count']+`</div>
			<div class='col-2 border'>`+(avg*(wkTime/3600)).toFixed(2)+`</div>
			`;
			$('.rows').append(row);
		}
	});
}