var move_point = 0;

function fetch(url)
{
    $.ajax({
    url: url ,
    ajaxStart: function(){
        $('body').append(loadingCard);
    },
    ajaxStop: function(){
        $('.card').remove();
    },
    }).fail(function() {
        showMessage("Coś poszło nie tak","warning");
    }).done(function(data) {
        $('#content-table').html('');
        data = JSON.parse(data);
        console.log(data);
        for(var i = 0 ; i < data.length; i++)
        {
            row = ` 
             <div onclick='currents(`+data[i]['id']+`)' class='col border machine' >`+data[i]['machine_name']+`</div>
            `;
            $('.machines').append(row);
        }
        $('.machine').click(function(){
            $('.machine').removeClass('activeMachine');
            $(this).addClass('activeMachine');
        });
        move_point = data[0]['move_point']
    });
}

function getCurrents(url){
    powers = 0;
    wkTime = 0;
    mvTime = 0;
    $('body').append(loadingCard);
   $.ajax({
    url: url,
    }).fail(function() {
        showMessage("Wystąpił problem podczas pobierania próbek urządzenia","warning");
        $('.card').remove();
    }).done(function(data) {
        $('.card').remove();
        $('.gaugs').addClass('d-flex');
        data = JSON.parse(data);

        for(i = 0; i < Object.keys(data).length-1 ; i++)
        {
            powers +=  power = data[i]['ampery'] * data[i]['volty'];
        
            if(power >= move_point)
            {
                mvTime++;
            }
            wkTime++;
        }

        avg = powers/Object.keys(data).length;

        percentColors = [[0.0, "#ffff00" ], [0.50, "#00ff00"], [1.0, "#ff0000"]]

        var opts = {
            
        angle: -0.2, // The span of the gauge arc
        lineWidth: 0.2, // The line thickness
        radiusScale: 1, // Relative radius
        pointer: {
            length: 0.6, // // Relative to gauge radius
            strokeWidth: 0.035, // The thickness
            color: '#000000' // Fill color
        },
        limitMax: false,     // If false, max value increases automatically if value > maxValue
        limitMin: false,     // If true, the min value of the gauge will be fixed
        percentColors:percentColors,
        generateGradient: true,
        highDpiSupport: true,     // High resolution support
    
        };
        var target_avg = document.getElementById('gauge-average'); // your canvas element

        var target_wktm = document.getElementById('gauge-work-time'); // your canvas element
    
        var target_mvtm = document.getElementById('gauge-move-time'); // your canvas element
    
        var target_sqct = document.getElementById('gauge-sequence-count'); // your canvas element
    
    
        var gauge_avg = new Gauge(target_avg).setOptions(opts); // create sexy gauge!
    
        var gauge_wktm = new Gauge(target_wktm).setOptions(opts); // create sexy gauge!
    
        var gauge_mvtm = new Gauge(target_mvtm).setOptions(opts); // create sexy gauge!
    
        var gauge_sqct = new Gauge(target_sqct).setOptions(opts); // create sexy gauge!
        avg = avg.toFixed(2);

        gauge_avg.maxValue = 2*avg; // set max gauge value,
        gauge_avg.set(avg); // set actual value
    
        gauge_wktm.maxValue = 2*(wkTime/3600).toFixed(2); // set max gauge value,
        gauge_wktm.set((wkTime/3600).toFixed(2)); // set actual value
    
        gauge_mvtm.maxValue = 2*(mvTime/3600).toFixed(2); // set max gauge value,
        gauge_mvtm.set((mvTime/3600).toFixed(2)); // set actual value
    
        gauge_sqct.maxValue = 2*data['sequenceCount']; // set max gauge value,
        gauge_sqct.set(data['sequenceCount']); // set actual value

        $('#avg_val').text(avg);
        $('#avg_val').css('margin-left','30%');
    
        $('#wktm_val').text((wkTime/3600).toFixed(2));
        $('#wktm_val').css('margin-left','30%');
    
        $('#mvtm_val').text((mvTime/3600).toFixed(2));
        $('#mvtm_val').css('margin-left','30%');
    
        $('#sqct_val').text(data['sequenceCount']);
        $('#sqct_val').css('margin-left','30%');
        
            
        
    });
}