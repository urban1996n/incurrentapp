<?php

namespace App\Application\Infrastructure\Http\View\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ViewController
 * @package App\Infrastructure\Http\View\Controller
 */
final class AccountViewController extends Controller
{

   /**
     * Render import form for creating request to POST Import into database
     * @Route("/login", name="login_view")
     */
    public function loginForm(){
        
        return $this->render('login/login.html.twig');
    }

    /**
     * Render import form for creating request to POST Import into database
     * @Route("/users", name="users_list")
     */
    public function usersIndex(){
        
        return $this->render('users/users.html.twig');
    }
}
