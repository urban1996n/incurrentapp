<?php

namespace App\Application\Infrastructure\Http\View\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CollationViewController
 * @package App\Infrastructure\Http\View\Controller
 */
final class CollationViewController extends Controller
{  
    
    /**
    * Render export form for creating request to POST Export into database
    * @Route("/collation/current/measure", name="current_measure")
    */
    public function curretMeasureCollation(){
        
        return $this->render('collations/currentMeasure.html.twig');
    }

   /**
     * Render export form for creating request to POST Export into database
     * @Route("/collation/historic", name="historic_data")
     */
    public function historicDataCollation(){
        
        return $this->render('collations/historicData.html.twig');
    }
    
    /**
     * Render export form for creating request to POST Export into database
     * @Route("/collation/current/chart", name="current_chart")
     */
    public function currentChartCollation(){
        
        return $this->render('collations/currentChart.html.twig');
    }
    
    /**
     * Render export form for creating request to POST Export into database
     * @Route("/collation/power_usage", name="power_usage")
     */
    public function powerUsageCollation(){
        
        return $this->render('collations/powerUsage.html.twig');
    }
    
    /**
     * Render export form for creating request to POST Export into database
     * @Route("/collation/overload_list", name="overload_list")
     */
    public function overloadList(){
        
        return $this->render('collations/overloadList.html.twig');
    }
}
