<?php

namespace App\Application\Infrastructure\Http\View\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ViewController
 * @package App\Infrastructure\Http\View\Controller
 */
final class SettingsViewController extends Controller
{

   /**
     * Render import form for creating request to POST Import into database
     * @Route("/machine/list", name="settings_index")
     */
    public function settingsList(){
        
        return $this->render('settings/index.html.twig');
    }
}
